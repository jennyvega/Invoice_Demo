package controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hibernate.boot.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dao.InvoicesDaoImpl;
import model.Customer;
import model.Invoices;
import model.MetaDataInvoice;
import util.ErrorHandling;

@RestController
public class InvoiceService {

	@Autowired
	InvoicesDaoImpl config;
	

	ErrorHandling errorCheck = new ErrorHandling();

	@RequestMapping(value = "v1/invoice", method = RequestMethod.POST, headers = "Accept=application/json"
			,produces = "application/json")
	@ResponseBody
	  ResponseEntity<Customer> newInvoice(@RequestBody Invoices newInvoice)  {
		
		
	boolean dateValid=	errorCheck.checkDateValid(newInvoice);
	
	if (dateValid==false)
	{
	return new ResponseEntity<>( HttpStatus.NOT_ACCEPTABLE);
	}
	
		Customer cust = config.insertInvoice(newInvoice);
		return new ResponseEntity<Customer>( cust,HttpStatus.CREATED);

	}

	
	
	
	
	@RequestMapping(value = "v1/invoice-details", method = RequestMethod.GET, 
					headers = "Accept=application/json",produces = "application/json")
	@ResponseBody
	ResponseEntity <MetaDataInvoice> searchInvoice(@RequestParam(value = "invoice_number", required = true) String invoice_number,
			@RequestParam(value="limit", required = false, defaultValue = "50") Integer limit, @RequestParam(value="offset", required = false, defaultValue = "0") Integer offset) {

		//Get DB results and check limit and offset
		List<Customer> invoices = config.getId(invoice_number).stream().skip(offset).limit(limit).collect(Collectors.toList());;
		MetaDataInvoice meta = new MetaDataInvoice(invoices, limit, offset);
		meta.setLimit(limit);
		meta.setOffset(offset);
		meta.setResponseList(invoices);
		
		if (invoices.size() >0)
		{
			
			 return new ResponseEntity<>(meta, HttpStatus.OK);
		}
		
		if(invoices.size() == 0)
		{
			 return new ResponseEntity<>(meta, HttpStatus.NO_CONTENT);
			
		}
		 
		    return new ResponseEntity<MetaDataInvoice>(meta, HttpStatus.OK);

	}




}
