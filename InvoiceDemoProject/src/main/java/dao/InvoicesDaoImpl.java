package dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import model.Customer;
import model.Invoices;
import util.Util;

@Component
public class InvoicesDaoImpl implements InvoicesDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	
	// GET RECORDS BASED ON INVOICE_NUMBER

	public List<Customer> getId(String id) {

		List<Customer> list = new ArrayList<Customer>();


		jdbcTemplate
				.query("SELECT id, po_number, invoice_number , due_date, amount_cents, created_at" + " "
						+ "FROM invoices WHERE invoice_number = ? ORDER BY id DESC",
				new Object[] { id },
				(rs, rowNum) -> new Customer(rs.getInt("id"), rs.getString("po_number"), rs.getString("invoice_number"),
						rs.getString("due_date"), rs.getInt("amount_cents"), rs.getString("created_at")))
				.forEach(customer -> list.add(customer));

		return list;

	}

	// INSERT INVOICE -- should go in Stored Procedure
	public Customer insertInvoice(Invoices invoice) {
		Customer cust = null;
		String util = new Util().getIsoTime();
		List<String> list = new ArrayList<String>();

		int check = jdbcTemplate.update(
				"INSERT INTO invoices "
						+ "(invoice_number,po_number,due_date,amount_cents, created_at) VALUES (?,?,?,?,?)",
				new Object[] { invoice.getInvoice_number(), invoice.getPo_number(), invoice.getDue_date(),
						invoice.getAmount_cents(), util }

		);

		if (check == 1)

		{

			cust = jdbcTemplate.queryForObject(
					"SELECT id, po_number, invoice_number , due_date, amount_cents, created_at" + " "
							+ "FROM invoices WHERE invoice_number = ? ORDER BY id DESC LIMIT 1",
					new Object[] { invoice.getInvoice_number() },
					(rs, rowNum) -> new Customer(rs.getInt("id"), rs.getString("po_number"),
							rs.getString("invoice_number"), rs.getString("due_date"), rs.getInt("amount_cents"),
							rs.getString("created_at")));

			return cust;
		}

		System.out.println(invoice.getId());
		System.out.println(invoice.getInvoice_number());
		System.out.println(invoice.getDue_date());
		System.out.println(invoice.getAmount_cents());
		return cust;

	}

	// CREATE AND POPULATE TABLE
	public void run(String... strings) throws Exception {

//		jdbcTemplate.execute("DROP TABLE invoices IF EXISTS");
		jdbcTemplate.execute("CREATE TABLE invoices(" + "id BIGINT auto_increment, "
				+ "invoice_number VARCHAR(64) NULL, " + "po_number VARCHAR(64) NULL," + "due_date datetime NULL,"
				+ "amount_cents BIGINT NULL," + "created_at VARCHAR(64) NULL)");

		// Split up the array of whole names into an array of first/last names
		List<Object[]> splitUpNames = Arrays.asList("333 yanilda", "Dean penny", "Bloch mickey", "Long another")
				.stream().map(name -> name.split(" ")).collect(Collectors.toList());

	
		splitUpNames.forEach(
				name -> System.out.println(String.format("Inserting invoices record for %s %s", name[0], name[1])));

		// Uses JdbcTemplate's batchUpdate operation to bulk load data
		jdbcTemplate.batchUpdate("INSERT INTO invoices (invoice_number,po_number) VALUES (?,?)", splitUpNames);

		System.out.println("Querying for invoices records where first_name = 'Josh':");
		jdbcTemplate
				.query("SELECT id, invoice_number, po_number FROM invoices WHERE po_number = ?", new Object[] { "id" },
						(rs, rowNum) -> new Customer(rs.getInt("id"), rs.getString("invoice_number"),
								rs.getString("po_number"), rs.getString("due_date"), rs.getInt("amount_cents"),
								rs.getString("created_at")))
				.forEach(customer -> System.out.println(customer.toString()));
	}
}
