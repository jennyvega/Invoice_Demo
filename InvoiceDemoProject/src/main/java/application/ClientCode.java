package application;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Customer;
import model.MetaDataInvoice;

public class ClientCode {

	public static void main(String[] args) throws URISyntaxException, JsonProcessingException {
		// TODO Auto-generated method stub

	insertInvoice();
	getInvoices("8756",0,5);
	
	}

	public static String buildJson() throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		Customer user = new Customer();
		user.setInvoice_number("1234");
		user.setPo_number("po1234");
		user.setDue_date("2017-10-10");
		user.setAmount_cents(300);

		// Convert object to JSON string
		String jsonInString = mapper.writeValueAsString(user);

		return jsonInString;

	}

	
static void	insertInvoice() throws URISyntaxException, JsonProcessingException
	{
		
	URI url = new URI("http://localhost:8080/service/v1/invoice");

	// Insert
	RestTemplate template = new RestTemplate();

	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);

	HttpEntity<String> entity = new HttpEntity<String>(buildJson(), headers);
	ResponseEntity<String> response = template.postForEntity(url, entity, String.class);
	System.out.println(response);
		
	}


 static void getInvoices(String invoice_number, int offset, int limit) throws URISyntaxException, JsonProcessingException
 {
	 
	 String url = "http://localhost:8080/service/v1/invoice-details?invoice_number={invoice_number}&offset={offset}&limit={limit}";

	 Map<String, Object> vars = new HashMap<String, Object>();
	 vars.put("invoice_number", invoice_number);
	 vars.put("offset", offset);
	 vars.put("limit", limit);
	

		RestTemplate template = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(buildJson(), headers);
		String response = template.getForObject(url, String.class, vars);
		System.out.println(response);
	 
	 
	 
 }
}
