package application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import controller.InvoiceService;
import dao.InvoicesDaoImpl;



@SpringBootApplication
@ComponentScan(basePackages = {
        "src.main.java.dao",
        "src.main.java.controller",
        "src.main.java.model",
        "src.main.java.util"}, basePackageClasses = {InvoicesDaoImpl.class,InvoiceService.class})
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired	
    InvoicesDaoImpl config;
    
    
    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	public void run(String... arg0) throws Exception {

		//Create table and populate
		config.run(arg0);
	}

   
}