package model;

import java.util.List;

import org.springframework.stereotype.Repository;


public class MetaDataInvoice {
	
	
	public MetaDataInvoice(List<Customer> list, int offset, int limit)
	{
		this.responseList=list;
		this.offset=offset;
		this.limit=limit;
		
	}

	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public List<Customer> getResponseList() {
		return responseList;
	}
	public void setResponseList(List<Customer> responseList) {
		this.responseList = responseList;
	}
	private int offset;
	private int limit;
	private List<Customer> responseList;
	
	
	
}
