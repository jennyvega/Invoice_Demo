package model;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;


public class Invoices {

	private Integer id;

	@NotEmpty
	private String invoice_number;

	private String po_number;

	@NotEmpty
	private String due_date;

	@NotEmpty
	private int amount_cents;

	public Invoices(int rowNum, String string, String string2) {
		// TODO Auto-generated constructor stub
	}

	public Invoices() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInvoice_number() {
		return invoice_number;
	}

	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}

	public String getPo_number() {
		return po_number;
	}

	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}

	public String getDue_date() {
		return due_date;
	}

	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}

	public int getAmount_cents() {
		return amount_cents;
	}

	public void setAmount_cents(int amount_cents) {
		this.amount_cents = amount_cents;
	}

}
