package model;

public class Customer {
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPo_number() {
		return po_number;
	}

	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}

	public String getInvoice_number() {
		return invoice_number;
	}

	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}

	private long id;
    private String po_number, invoice_number;
    public String getDue_date() {
		return due_date;
	}

	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}

	public int getAmount_cents() {
		return amount_cents;
	}

	public void setAmount_cents(int amount_cents) {
		this.amount_cents = amount_cents;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	private String due_date;

	int amount_cents ;

	String created_at;

    public Customer(int l, String po_number, String invoice_number, String due_date, int amount, String created_at) {
        this.id = l;
        this.po_number = po_number;
        this.invoice_number = invoice_number;
    	this.due_date = due_date;
this.amount_cents = amount;

    	this.created_at= created_at;
    }

    public Customer() {
		// TODO Auto-generated constructor stub
	}

	@Override
    public String toString() {
        return String.format(
                "Customer[id=%d, po_number='%s', invoice_number='%s']",
                id, po_number, invoice_number);
    }

    // getters & setters omitted for brevity
}

