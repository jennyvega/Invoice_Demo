/**
 * 
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import application.Application;
import dao.InvoicesDaoImpl;
import model.Customer;
import model.Invoices;


/**
 * @author only2dhir
 *
 */

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(application.Application.class)
public class DaoTest {

	@Autowired
	private InvoicesDaoImpl userDao;

	@Test
	public void createUser() {
		
		Invoices inv = new Invoices();
		inv.setAmount_cents(300);
		inv.setInvoice_number("1234");
		
		Customer savedUser = userDao.insertInvoice(inv);
		
		assertEquals(savedUser.getAmount_cents(), 300);

	}

	@Test
	public void findAllUsers() {
		List<Customer> users = userDao.getId("1234");
		assertNotNull(users);
		assertTrue(users.size() > 0);
	}

	

}
