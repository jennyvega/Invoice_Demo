package src.main.test;

import static org.junit.Assert.*;

import java.net.URI;


import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Customer;
import model.MetaDataInvoice;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(application.Application.class)
public class ControllerTest {

	String jsonString = null;
	String invoice_number;
	int offset;
	int limit;
	
	@Before
	public void getJson() throws JsonProcessingException 
	{
		
		ObjectMapper mapper = new ObjectMapper();

		Customer user = new Customer();
		user.setInvoice_number("1234");
		user.setPo_number("po1234");
		user.setDue_date("2017-10-10");
		user.setAmount_cents(300);

		// Convert object to JSON string
		String jsonInString = mapper.writeValueAsString(user);
		jsonString =jsonInString;
		invoice_number = "1234";
		limit =1;
		offset = 0;
			
		
	}

	@Test
	public void buildJson() throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		Customer user = new Customer();
		user.setInvoice_number("1234");
		user.setPo_number("po1234");
		user.setDue_date("2017-10-10");
		user.setAmount_cents(300);

		// Convert object to JSON string
		String jsonInString = mapper.writeValueAsString(user);
		
		System.out.println(jsonInString);

		assertEquals("1234",user.getInvoice_number());
		assertNotNull(jsonInString);
		

	}

	@Test
 public void	insertInvoice() throws URISyntaxException, JsonProcessingException
	{
		
	URI url = new URI("http://localhost:8080/service/v1/invoice");

	// Insert
	RestTemplate template = new RestTemplate();

	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);

	HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
	ResponseEntity<String> response = template.postForEntity(url, entity, String.class);
	System.out.println(response);
	
	assertTrue(response.getStatusCode().is2xxSuccessful());
		
	}


	@Test
 public void getInvoices() throws URISyntaxException, JsonProcessingException
 {
	 
	 String url = "http://localhost:8080/service/v1/invoice-details?invoice_number={invoice_number}&offset={offset}&limit={limit}";

	 Map<String, Object> vars = new HashMap<String, Object>();
	 vars.put("invoice_number", invoice_number);
	 vars.put("offset", offset);
	 vars.put("limit", limit);
	

		RestTemplate template = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
		String response = template.getForObject(url, String.class, vars);
		
		assertTrue(!response.isEmpty());
	 
	 
	 
 }
}
