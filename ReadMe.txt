Resources

POST: http://localhost:8080/service/v1/invoice
Example : {"invoice_number":"8756","po_number":"test","due_date":"2017-10-10","amount_cents":300"}
Application/JSON Header

GET: http://localhost:8080/service/v1/invoice-details?invoice_number=8756
accepts limit & offset. 

Build a jar and run in your local machine. Uses an embedded database, creates a table upon start up. 

DB Connectivity can be changed under application.properties 

Contains both REST web service and Client Code using Spring and Spring-Boot.